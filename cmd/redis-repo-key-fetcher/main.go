package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"os"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/vmihailenco/msgpack"
)

var ctx = context.Background()

type RepositoryStatus string

type Repository struct {
	ID              int64
	NamespaceID     int64
	Name            string
	Path            string
	ParentID        sql.NullInt64
	MigrationStatus RepositoryStatus
	MigrationError  sql.NullString
	CreatedAt       time.Time
	UpdatedAt       sql.NullTime
	DeletedAt       sql.NullTime
	Size            *int64
}

func main() {
	colorGreen := "\033[32m"
	colorWhite := "\033[37m"
	argsWithoutProg := os.Args[1:]
	key := argsWithoutProg[0]
	var repo *Repository = new(Repository)
	rdb := RedisClient()
	rScmd := rdb.Get(ctx, key)
	bytes, err := rScmd.Bytes()
	if err != nil {
		panic(err)
	}
	err = msgpack.Unmarshal(bytes, repo)
	if err != nil {
		panic(err)
	}

	b, err := json.MarshalIndent(repo, "", "    ")
	if err != nil {
		panic(err)
	}
	fmt.Printf("Redis key %s changed. The value is now:\n", string(colorWhite)+key)
	fmt.Println(string(colorGreen), string(b))
}

func RedisClient() *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "asecret",
		DB:       0, // use default DB
	})
}
